# docker4yocto



## Getting started

```
# clone the repo
git clone https://gitlab.com/rehman.anees/docker4yocto.git

cd docker4yocto

# build a docker image
./yoctobuilder -c build

# run the conatiner; the <workdir> is the build directory
./yoctobuilder -c run -w <workdir>
```
