#!/bin/bash

set -e

SCRIPT_NAME=${0##*/}
readonly SCRIPT_VERSION="0.1"

#### global variables ####
readonly PARAM_DEFAULT_TAG="yocto/build:1"
readonly PARAM_DEFAULT_CONTAINER="yocto-builder"
readonly PARAM_DEFAULT_RECIPE="Dockerfile"

#### Input params ####
PARAM_CMD=""
PARAM_NAME="na"
PARAM_RECIPE="na"
PARAM_TAG="na"
PARAM_WORKDIR="na"

pr_info() {
    echo ""
    echo "[I] $1"
    echo ""
}

pr_error() {
    echo ""
    echo "[E] $1"
    echo ""
}

### usage ###
usage() {
    echo "This program version ${SCRIPT_VERSION}"
    echo " Docker command wrapper for building/running yocto builder"
    echo ""
    echo "Usage:"
    echo " ./${SCRIPT_NAME} options"
    echo ""
    echo "Options:"
    echo "  -h|--help   -- print this help"
    echo "  -c|--cmd <command>"
    echo "     Supported commands:"
    echo "       attach  -- attach to running container"
    echo "       build   -- build docker a image"
    echo "       start   -- start running a container"
    echo "       run     -- create/run a container interactively"
    echo "  -n|--name    -- name of the container[default: yocto-builder]"
    echo "  -r|--recipe  -- path to Dockerfile"
    echo "  -t|--tag     -- tag name for the container image[default: yocto/build:1]"
    echo "  -w|--workdir -- create a container image with shared workspace"
    echo "Examples of use:"
    echo "  Build docker image: ./${SCRIPT_NAME} -c build -t <tagname> -r <Dockerfile>"
    echo "  create docker container : ./${SCRIPT_NAME} -c run -w <workdir>"
    echo "  Start a docker container: ./${SCRIPT_NAME} -c start"
    echo ""
}

#### parse input arguments ####
readonly SHORTOPTS="hn:r:t:w:c:"
readonly LONGOPTS="cmd:,help,name:,recipe:,tag:,workdir:"


ARGS=$(getopt -s sh --options ${SHORTOPTS}  \
      --longoptions ${LONGOPTS} --name ${SCRIPT_NAME} -- "$@" )

eval set -- "$ARGS"

while true; do
    case $1 in
        -c|--cmd ) # script command
            shift
            PARAM_CMD=${1};
            ;;
        -n|--name )
            shift
            PARAM_NAME=${1};
            ;;
        -r|--recipe )
            shift
            [ -e ${1} ] && {
                PARAM_RECIPE=${1};
            };
            ;;
        -t|--tag )
            shift
            PARAM_TAG=${1};
            ;;
        -w|--workdir )
            shift
            [ -d ${1} ] && {
                PARAM_WORKDIR=${1};
            };
            ;;
        -h|--help ) # get help
            usage
            exit 0;
            ;;
        -- )
            shift
            break
            ;;
        * )
            shift
            break
            ;;
    esac
    shift
done

function fn_attach {
    LPARAM_NAME=${PARAM_NAME}

    [ "${LPARAM_NAME}" = "na" ] && {
        LPARAM_NAME=${PARAM_DEFAULT_CONTAINER}
    }

    pr_info "Attach to container: ${LPARAM_NAME}"
    docker attach ${LPARAM_NAME}
}

function fn_build {
    LPARAM_TAG=${PARAM_TAG}

    [ "${LPARAM_TAG}" = "na" ] && {
        LPARAM_TAG=${PARAM_DEFAULT_TAG}
    }

    LPARAM_RECIPE=${PARAM_RECIPE}

    [ "${LPARAM_RECIPE}" = "na" ] && {
        LPARAM_RECIPE=${PARAM_DEFAULT_RECIPE}
    }

    docker build \
           --build-arg UUID="$(id -u)" \
           --build-arg UGID="$(id -g)" \
           --build-arg UNAME="$(whoami)" \
           . -t ${LPARAM_TAG} -f ${LPARAM_RECIPE}
}

function fn_start {
    LPARAM_NAME=${PARAM_NAME}

    [ "${LPARAM_NAME}" = "na" ] && {
        LPARAM_NAME=${PARAM_DEFAULT_CONTAINER}
    }

    pr_info "Starting stopped container"
    docker start -i ${LPARAM_NAME}
}

function fn_run {
    [ -d ${PARAM_WORKDIR} ] || {
        pr_error "workspace: ${PARAM_WORKDIR} does not exist, provide a valid path"
        exit 1
    }

    LPARAM_TAG=${PARAM_TAG}

    [ "${LPARAM_TAG}" = "na" ] && {
        LPARAM_TAG=${PARAM_DEFAULT_TAG}
    }

    LPARAM_NAME=${PARAM_NAME}

    [ "${LPARAM_NAME}" = "na" ] && {
        LPARAM_NAME=${PARAM_DEFAULT_CONTAINER}
    }

    docker run \
           -it \
           --volume="${PARAM_WORKDIR}:/yocto" \
           --env="DISPLAY" \
           --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
           --env=USER=${USER} \
           --name=${LPARAM_NAME} \
           ${LPARAM_TAG}
}

RET_CODE=0;

pr_info "Command: \"${PARAM_CMD}\" start..."

case $PARAM_CMD in
    attach )
        fn_attach || {
            RET_CODE=1
        }
        ;;
    build )
        fn_build || {
            RET_CODE=1
        }
        ;;
    start )
        fn_start || {
            RET_CODE=1
        }
        ;;
    run )
        fn_run || {
            RET_CODE=1
        }
        ;;
    * )
        pr_error "Invalid input command: \"${PARAM_CMD}\"";
        RET_CODE=1;
        ;;
esac

pr_info "Command: \"${PARAM_CMD}\" end. Exit code: ${RET_CODE}"

exit ${RET_CODE};
