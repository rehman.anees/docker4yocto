FROM debian:11

ARG UUID
ARG UGID
ARG UNAME

RUN apt-get clean && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
        build-essential \
        chrpath \
        cpio \
        diffstat \
        fluxbox \
        gawk \
        git-core \
        locales \
        lz4 \
        procps \
        python \
        python3 \
        python3-pip \
        screen \
        socat \
        subversion \
        sudo \
        sysstat \
        texinfo \
        tightvncserver \
        tmux \
        unzip \
        wget \
        vim \
        xz-utils \
        zstd && \
        pip3 install kas

RUN which dash &> /dev/null && (\
    echo "dash dash/sh boolean false" | debconf-set-selections && \
    DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash) || \
    echo "Skipping dash reconfigure (not applicable)"


RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8

RUN groupadd -g ${UGID} -o ${UNAME}
RUN useradd -m -u ${UUID} -g ${UGID} -o -s /bin/bash ${UNAME}
RUN echo 'ALL ALL = (ALL) NOPASSWD: ALL' >> /etc/sudoers

WORKDIR /yocto

RUN mkdir -p /usr/local/bin
COPY entrypoint.sh /usr/local/bin
RUN chmod +x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
